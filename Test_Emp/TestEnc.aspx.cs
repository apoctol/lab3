﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Test_Emp
{
    public partial class TestEnc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void BC(object sender, EventArgs e)
        {
            (sender as Button).PostBackUrl = "~/Default.aspx";
            (sender as Button).Text = "Уверены?";
        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            String savePath = @"c:\temp\temp.txt";
            FileUpload1.SaveAs(savePath);
            Dictionary<int, string> pairs = new Dictionary<int, string>();
            for (int i = 0; i <=33; i++)
            {
               pairs.Add(i, Program.TestEncription(i));
            }
            Bild.Text = "[Смещение, Пример]"+ "<p></p>";
            foreach (var item in pairs)
            {
                Bild.Text = Bild.Text + item + "<p></p>";
            }
        }

    }
}