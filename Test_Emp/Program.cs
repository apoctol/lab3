﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Test_Emp
{
    public class Program
    {
        public static string RunEncription(int setter)
        {
            Program.offset= setter;
            string result = CharRewrite(Reader());
            Writer(result);
            return result;
        }
        public static string TestEncription(int setter)
        {
            Program.offset = setter;
            string result = CharRewrite(TestReader());
            return result;
        }
        static int offset; //Вводится в ручную. Задаёт шаг смещения
        private static string TestReader()
        {
            string path = @"c:\temp\temp.txt";
            string text = null;
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                text = sr.ReadLine();
            }
            return text;
        }
        private static void Writer(string result)
        {

            using (FileStream fs = File.Create(@"c:\temp\temp2.txt"))
            {
                fs.Close();
                File.WriteAllText(@"c:\temp\temp2.txt", result);
            }
        }
        private static string Reader()
        {
            string path = @"c:\temp\temp.txt";
            string text = null;
            // Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            //var fonts = ConsoleHelper.ConsoleFonts;
            //ConsoleHelper.SetConsoleFont(2);
            // ConsoleHelper.SetConsoleIcon(SystemIcons.Information);
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    text = text+line;
                }
            }
            return text;
        } // Парсит слова из тхт
        private static char CharEncripter (char letter) //Смещает буквы на заданное число
        {
            string alfabet = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
            int nomer = 0;
            if (alfabet.Contains(letter))
            {
                for (int i = 0; i <= alfabet.Length; i++)
                {

                    if (alfabet[i].Equals(letter))
                    {
                        nomer = i;
                        break;
                    }
                }
                nomer = nomer + Program.offset*2;
                while (nomer>65 || nomer < 0)
                {
                    if (nomer > 65)
                    {
                        nomer = nomer - 66;
                    }
                    if (nomer<0)
                    {
                        nomer = +66;
                    }
                    
                }
                return alfabet[nomer];
            }
            else
            {
                return letter;
            }
            
        }
        private static string CharRewrite(string MasLet) //создаёт строку смещённых знаков
        {
            try
            {
                string ret = null;
                for (int i = 0; i < MasLet.Length; i++)
                {
                    ret = ret + CharEncripter(MasLet[i]);
                }
                return ret;
            }
            catch (Exception)
            {

                return "Файл отсутствует или имеет неверный формата";
            }
                
        }
        public static string CharEncripter_Test(int setter, char letter)
        {
            Program.offset = setter;
            return CharEncripter(letter).ToString();
        }
    }
}