﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Test_Emp
{
    public partial class Encripted : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
      
        protected void BC2(object sender, EventArgs e)
        {
            (sender as Button).Text = "Уверены?";
            (sender as Button).PostBackUrl = "~/TestEnc.aspx";
        }
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            String savePath = @"c:\temp\temp.txt";
            int num;
            //Pole.Text = "0";
            if (FileUpload1.FileName != null) //Начиная от сих, Должна быть проверка на пустую ссылку и буквы в поле. 
            {
                FileUpload1.SaveAs(savePath); 
                if (int.TryParse(Pole.Text, out num))
                {
                    Bild.Text = Program.RunEncription(num);
                }
                else
                {
                    Pole.Text = "Вводить можно только цифры";
                }
            }
            else
            {
                Bild.Text = "Файл отсутствует или неверного формата";// Однако именно эту запись я не увидел ни разу
            }                                                        // Я так понимаю, что там всегда не null, но блок оставил на случай если ошибаюсь
        }

        protected void Pole_TextChanged(object sender, EventArgs e)
        {

        }

        protected void DownloadButton_Click(object sender, EventArgs e)
        {
            try
            {
            string MyPath = HttpContext.Current.Server.MapPath("");
            //int Путь = MyPath.Length + 1;
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=enc.txt"); //+ this.TextBox1.Text.Substring(Путь)+"");
            Response.ContentType = "application/octet-strean";
            string importAddress = @"c:\temp\temp2.txt";
            using (var web = new WebClient())
            using (var importStream = web.OpenRead(importAddress))
            {importStream.CopyTo(Response.OutputStream);}
            Response.End();
            }
            catch (Exception)
            {

                Bild.Text = "Я пустой!!!";
            }
        }
    }
}